package com.swat_training.springapp.service;

import java.io.Serializable;
import java.util.List;
import com.swat_training.springapp.domain.Product;

public interface ProductManager extends Serializable {

    public void increasePrice(int percentage);
    
    public List<Product> getProducts();

}